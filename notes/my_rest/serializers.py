
from django.forms import widgets
from rest_framework import serializers
from my_rest.models import Note, Color, Category, Label, Media_Note
from django.contrib.auth.models import User


class UserSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=100)
  

class ColorSerializer(serializers.ModelSerializer):
    color = serializers.StringRelatedField(many=True)

    class Meta:   
      model = Color
      fields = ('code', 'color')
          

class LabelSerializer(serializers.ModelSerializer):
    
    #label_name = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    class Meta: 
        model = Label
        fields = ('label_name',)
   

class MediaSerializer(serializers.ModelSerializer):
   # my_file = serializers.FileField(max_length=255)
   
    class Meta: 
        model = Media_Note
        fields = ['my_file',]
        
class CategorySerializer(serializers.ModelSerializer):

    category_name = serializers.CharField(max_length=255)
    Child = serializers.StringRelatedField(many=True)

    class Meta: 
        model = Category
        fields = ['category_name','Child']
        
# class NoteSerializer(serializers.Serializer):
#   pk = serializers.IntegerField(read_only=True)
#   title = serializers.CharField(required=False, max_length=30)


class NoteSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(read_only=True)
    #title = serializers.CharField(required=False, max_length=30)
    user = UserSerializer(required=False, read_only=True)
    #content = serializers.CharField(max_length=200)
    note_media = MediaSerializer(many=True)
    note_category = CategorySerializer(many=True)
    note_label = LabelSerializer(many=True)

    class Meta:
        model = Note
        fields = ['pk','title','content','note_category','color','note_label', 'user','note_media']
    