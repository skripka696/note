from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from models import Note
from my_rest.serializers import  NoteSerializer,ColorSerializer, LabelSerializer, CategorySerializer
from models import Note, Category, Color, Label
from rest_framework import mixins
from rest_framework import generics

from rest_framework import viewsets 
from rest_framework.decorators import detail_route
from rest_framework import permissions


class NoteViewSet(viewsets.ModelViewSet):
    """
    Returns a list of notes.
    Edit, delete and add new ones.

    For more details about all category [see here][ref].
    [ref]: http://127.0.0.1:8000/api/category/
    
    For more details about all color [see here][col].
    [col]: http://127.0.0.1:8000/api/color/

    For more details about all label [see here][lab].
    [lab]: http://127.0.0.1:8000/api/label/
    """
    queryset = Note.objects.all()
    serializer_class = NoteSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

   

class ColorViewSet(viewsets.ModelViewSet):
    """
    Returns a list of color.
    Edit, delete and add new ones.

    For more details about all category [see here][ref].
    [ref]: http://127.0.0.1:8000/api/category/
    
    For more details about all notes [see here][not].
    [not]: http://127.0.0.1:8000/api/note/

    For more details about all label [see here][lab].
    [lab]: http://127.0.0.1:8000/api/label/
    """
    queryset = Color.objects.all()
    serializer_class = ColorSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class CategoryViewSet(viewsets.ModelViewSet):
    """
    Returns a list of category.
    Edit, delete and add new ones.

    For more details about all note [see here][not].
    [not]: http://127.0.0.1:8000/api/note/
    
    For more details about all color [see here][col].
    [col]: http://127.0.0.1:8000/api/color/

    For more details about all label [see here][lab].
    [lab]: http://127.0.0.1:8000/api/label/
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class LabelViewSet(viewsets.ModelViewSet):
    """
    Returns a list of label.
    Edit, delete and add new ones.

    For more details about all category [see here][ref].
    [ref]: http://127.0.0.1:8000/api/category/
    
    For more details about all color [see here][col].
    [col]: http://127.0.0.1:8000/api/color/

    For more details about all note [see here][not].
    [not]: http://127.0.0.1:8000/api/note/
    """
    queryset = Label.objects.all()
    serializer_class = LabelSerializer    
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)





