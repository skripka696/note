from django.db import models
from colorful.fields import RGBColorField
from django.contrib.auth.models import User
import mptt
# from mptt.fields import TreeForeignKey
#from mptt.models import MPTTModel

class Color(models.Model):
    code = RGBColorField()
    
    def __unicode__(self):
        return '{0}'.format( self.code)


class Category(models.Model):
    category_name = models.CharField(max_length=255)
    parent = models.ForeignKey('self', blank=True, null=True,
                               verbose_name="Parent", related_name='Child')

    def __unicode__(self):
        return '{0}'.format(self.category_name)

mptt.register(Category,)

class Label(models.Model):
    label_name = models.CharField(max_length=20) 

    def __unicode__(self):
        return '{0}'.format(self.label_name)


class Media_Note(models.Model):
    my_file = models.FileField(upload_to = 'media/', default="note.jpg")

    def __unicode__(self):
        return '{0}'.format(self.my_file)


class Note(models.Model):
    title = models.CharField(max_length=30)
    content = models.CharField(max_length=255)
    color = models.ForeignKey(Color, blank=True , null=True, related_name='color')
    note_category = models.ManyToManyField(Category)
    note_label = models.ManyToManyField(Label)
    note_media = models.ManyToManyField(Media_Note)
    user = models.ForeignKey(User)
   # list_category = []
 
    def __unicode__(self):
        return '{0} - {1}'.format(self.title, self.content)

  
# list_category = []
# for i in Note.objects.all():
#     category = Note.note_category
#     print category
#     list_category.append(i)
# print list_category
   

