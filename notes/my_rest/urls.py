"""notes URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from my_rest import views


list_note = views.NoteViewSet.as_view({
    'get': 'list',
    'post':'create'
    })

note_detail = views.NoteViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
    })

list_color = views.ColorViewSet.as_view({
    'get': 'list',
    'post':'create'
    })

color_detail = views.ColorViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
    })

list_category = views.CategoryViewSet.as_view({
    'get': 'list',
    'post':'create'
    })

category_detail = views.CategoryViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
    })

list_label = views.LabelViewSet.as_view({
    'get': 'list',
    'post':'create'
    })

label_detail = views.LabelViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
    })

urlpatterns = [
    url(r'^note/$', list_note),
    url(r'^note/(?P<pk>\d+)/$', note_detail),
    url(r'^color/$', list_color),
    url(r'^color/(?P<pk>\d+)/$', color_detail),
    url(r'^category/$', list_category),
    url(r'^category/(?P<pk>\d+)/$', category_detail),
    url(r'^label/$', list_label),
    url(r'^label/(?P<pk>\d+)/$', label_detail),

 ]